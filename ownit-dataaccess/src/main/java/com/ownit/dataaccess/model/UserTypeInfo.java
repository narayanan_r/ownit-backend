package com.ownit.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

import java.util.List;


/**
 * The persistent class for the user_type_info database table.
 * 
 */
@Entity
@Table(name="user_type_info")
@Data
@NamedQuery(name="UserTypeInfo.findAll", query="SELECT u FROM UserTypeInfo u")
public class UserTypeInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
	private Long id;

	@Column(name="user_type")
	private String userType;

	
	
}