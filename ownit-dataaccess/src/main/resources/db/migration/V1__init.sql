DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `signed_form` blob,
  PRIMARY KEY (`ID`),
  KEY `fk_user_ty` (`user_type_id`),
  CONSTRAINT `fk_user_ty` FOREIGN KEY (`user_type_id`) REFERENCES `user_info` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_info` */


/*Table structure for table `user_type_info` */

DROP TABLE IF EXISTS `user_type_info`;

CREATE TABLE `user_type_info` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `login_info`;

CREATE TABLE `login_info` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_info_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_user_log` (`user_info_id`),
  CONSTRAINT `fk_user_log` FOREIGN KEY (`user_info_id`) REFERENCES `user_info` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;